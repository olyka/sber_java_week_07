package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.SymbolImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

import static org.junit.Assert.*;
import static ru.edu.SymbolToFile.FILE_EXTENSION;
import static ru.edu.SymbolToFile.PRICE_HISTORY_PATH;

public class SymbolToFileTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SymbolToFileTest.class);

    public static final String SOME_TEST_SYMBOL = "TEST";

    @Test
    public void saveTest() throws IOException {

        SymbolImpl otherSymbol = new SymbolImpl();
        otherSymbol.setSymbol("123");
        otherSymbol.setPrice(new BigDecimal(123));
        otherSymbol.setTimestamp(Instant.now());

        SymbolImpl oldSymbol = new SymbolImpl();
        oldSymbol.setSymbol(SOME_TEST_SYMBOL);
        oldSymbol.setPrice(new BigDecimal(123));
        oldSymbol.setTimestamp(Instant.now());

        SymbolImpl newSymbol1 = new SymbolImpl();
        newSymbol1.setSymbol(SOME_TEST_SYMBOL);
        newSymbol1.setPrice(new BigDecimal(123));
        newSymbol1.setTimestamp(Instant.now());

        SymbolImpl newSymbol2 = new SymbolImpl();
        newSymbol2.setSymbol(SOME_TEST_SYMBOL);
        newSymbol2.setPrice(new BigDecimal(2000));
        newSymbol2.setTimestamp(Instant.now());

        SymbolImpl newSymbol3 = new SymbolImpl();
        newSymbol3.setSymbol(SOME_TEST_SYMBOL);
        newSymbol3.setPrice(new BigDecimal(10));
        newSymbol3.setTimestamp(Instant.now());

        SymbolToFile.save(otherSymbol, oldSymbol);
        SymbolToFile.save(oldSymbol, newSymbol1);
        SymbolToFile.save(newSymbol1, newSymbol2);
        SymbolToFile.save(newSymbol2, newSymbol3);


        assertTrue(!(new File(PRICE_HISTORY_PATH
                + otherSymbol.getSymbol()
                + FILE_EXTENSION).isFile()));

        File file = new File(PRICE_HISTORY_PATH
                + oldSymbol.getSymbol()
                + FILE_EXTENSION);
        assertTrue(file.isFile());


        Path path = Paths.get(file.getPath());
        try (BufferedReader reader = Files.newBufferedReader(path,
                Charset.forName("UTF-8"))) {
            String currentLine = null;
            while((currentLine = reader.readLine()) != null) {
                LOGGER.info(currentLine);
            }
        }

    }
}