package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class WatcherTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WatcherTest.class);

    public static final String SYMBOL_NAME = "BTCUSDT";

    public static final int MILLIS_TO_SLEEP = 3000;

    private SymbolPriceService service = mock(SymbolPriceService.class);

    @Test
    public void testMockRun() {

        Watcher newWatcher = new Watcher(SYMBOL_NAME, service);
        assertNotNull(newWatcher);

        Thread thread = new Thread(newWatcher);
        thread.start();
        LOGGER.info("Run Watcher with mocked service class.");

        try {
            Thread.sleep(MILLIS_TO_SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();

    }

    @Test
    public void testRealRun() {

        SymbolPriceService binanceService = new BinanceSymbolPriceService();
        Watcher newWatcher = new Watcher(SYMBOL_NAME, binanceService);
        assertNotNull(newWatcher);

        Thread thread = new Thread(newWatcher);
        thread.start();
        LOGGER.info("Run Watcher with real Binance service class.");

        try {
            Thread.sleep(MILLIS_TO_SLEEP * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();

    }

}