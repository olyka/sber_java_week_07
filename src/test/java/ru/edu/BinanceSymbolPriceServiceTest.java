package ru.edu;

import org.junit.Test;
import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

public class BinanceSymbolPriceServiceTest  {

    public static final String SOME_CURRENCY_SYMBOL = "BTCUSDT";
    SymbolPriceService service = new BinanceSymbolPriceService();
    Symbol price;

    @Test
    public void getPriceTest() {

        price = service.getPrice(SOME_CURRENCY_SYMBOL);

        assertNotNull(price);
        assertNotNull(price.getPrice());
        assertEquals(SOME_CURRENCY_SYMBOL, price.getSymbol());
        assertTrue(Instant.now().minus(
                        1,
                        ChronoUnit.SECONDS)
                .isBefore(price.getTimeStamp()));
    }

    @Test(expected = RuntimeException.class)
    public void errorAddressGetPriceTest() {
        price = service.getPrice(SOME_CURRENCY_SYMBOL, true);
    }

    @Test(expected = RuntimeException.class)
    public void errorSymbolGetPriceTest() {
        price = service.getPrice(SOME_CURRENCY_SYMBOL + "12345");
    }

    @Test(expected = RuntimeException.class)
    public void errorParametersGetPriceTest() {
        price = service.getPrice(SOME_CURRENCY_SYMBOL + "&asd=123");
    }

}