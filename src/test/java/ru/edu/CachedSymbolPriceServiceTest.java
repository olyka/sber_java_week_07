package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;
import static org.junit.Assert.*;

public class CachedSymbolPriceServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CachedSymbolPriceServiceTest.class);
    public static final String SYMBOL_NAME = "BTCUSDT";

    private SymbolPriceService binanceService = new BinanceSymbolPriceService();
    private SymbolPriceService cachedService = new CachedSymbolPriceService(binanceService);

    @Test
    public void getPriceTest() {

        Symbol price = cachedService.getPrice(SYMBOL_NAME);
        Symbol priceAgain = cachedService.getPrice(SYMBOL_NAME);
        assertEquals(price.getTimeStamp(), priceAgain.getTimeStamp());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Testing debug warning: {}", price);
        }

        final long timeout = 11_000L;
        LOGGER.info("Waiting to cache to clear: {} milliseconds", timeout);

        try {
            Thread.sleep(timeout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Symbol priceNew = cachedService.getPrice(SYMBOL_NAME);
        assertTrue(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));

    }

}