package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {

    /**
     * Cryptocurrency symbol.
     */
    private String symbol;

    /**
     * Cryptocurrency current price.
     */
    private BigDecimal price;

    /**
     * Timestamp of getting cryptocurrency price.
     */
    private Instant timestamp;

    /**
     * Set cryptocurrency symbol.
     * @param smbl Cryptocurrency symbol
     */
    public void setSymbol(final String smbl) {
        this.symbol = smbl;
    }

    /**
     * Set cryptocurrency price.
     * @param prc Cryptocurrency price
     */
    public void setPrice(final BigDecimal prc) {
        this.price = prc;
    }

    /**
     * Set timestamp of getting cryptocurrency price.
     * @param tmstmp Timestamp of getting the price
     */
    public void setTimestamp(final Instant tmstmp) {
        this.timestamp = tmstmp;
    }

    /**
     * Get cryptocurrency symbol.
     * @return Symbol
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Get cryptocurrency price.
     * @return Price
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Время получения данных.
     *
     * @return Timestamp
     */
    @Override
    public Instant getTimeStamp() {
        return timestamp;
    }

    /**
     * Print to string cryptocurrency info.
     * @return String
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                + "symbol='" + symbol + '\''
                + ", price=" + price
                + ", timestamp=" + timestamp
                + "}";
    }

}
