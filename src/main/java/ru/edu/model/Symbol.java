package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {

    /**
     * Get cryptocurrency symbol.
     * @return Symbol
     */
    String getSymbol();

    /**
     * Get cryptocurrency price.
     * @return Price
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return Timestamp
     */
    Instant getTimeStamp();
}
