package ru.edu;

public final class ManualThreads {

    /**
     * Cryptocurrency symbols.
     */
    public static final String[] CRYPTOCURRENCY_SYMBOLS = {
            "BTCUSDT",
            "BTCRUB",
            "ETHUSDT",
            "ETHRUB"};

    private ManualThreads() {
    }

    /**
     * Description.
     * @param args Args line
     */
    public static void main(final String[] args) {

        SymbolPriceService service = new BinanceSymbolPriceService();
        SymbolPriceService cached = new CachedSymbolPriceService(service);

        Thread[] threads = new Thread[CRYPTOCURRENCY_SYMBOLS.length];

        for (int i = 0; i < CRYPTOCURRENCY_SYMBOLS.length; i++) {
            threads[i] = new Thread(
                    new Watcher(CRYPTOCURRENCY_SYMBOLS[i], cached));
            threads[i].start();
        }

        try {
            threads[0].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
