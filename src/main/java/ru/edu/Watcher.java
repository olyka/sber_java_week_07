package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

import java.io.IOException;

public class Watcher implements Runnable {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Watcher.class);

    /**
     * Time to sleep.
     */
    public static final long MILLIS_TO_SLEEP = 3_000L;

    /**
     * Symbol object with previous data.
     */
    private Symbol lastData;

    /**
     * Symbol to watch.
     */
    private final String symbolToWatch;

    /**
     * SymbolPriceService we are using.
     */
    private final SymbolPriceService service;

    /**
     * Constructor.
     * @param smblToWatch Symbol to watch
     * @param srvc SymbolPriceService we are using
     */
    public Watcher(final String smblToWatch,
                   final SymbolPriceService srvc) {
        this.symbolToWatch = smblToWatch;
        this.service = srvc;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        Symbol newData;

        while (true) {

            newData = service.getPrice(symbolToWatch);
            LOGGER.info("Get an answer: {}", newData);

            try {
                SymbolToFile.save(lastData, newData);
            } catch (IOException e) {
                e.printStackTrace();
            }

            lastData = newData;

            try {
                Thread.sleep(MILLIS_TO_SLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
