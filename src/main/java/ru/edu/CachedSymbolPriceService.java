package ru.edu;

import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CachedSymbolPriceService implements SymbolPriceService {

    /**
     * Seconds.
     */
    public static final int CACHE_TIME_SLOT = 10;

    /**
     * Sleep.
     */
    public static final int MILLIS_TO_SLEEP = 3000;

    /**
     * SymbolPriceService object.
     */
    private final SymbolPriceService delegate;

    /**
     * Synchronized HashMap collection for Symbol objects.
     */
    private final Map<String, Symbol> cache =
            Collections.synchronizedMap(new HashMap<>());

    /**
     * Constructor.
     * @param smblPriceService SymbolPriceService object
     */
    public CachedSymbolPriceService(
            final SymbolPriceService smblPriceService) {
        this.delegate = smblPriceService;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName Some symbol
     * @return Symbol object
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        return getPrice(symbolName, false);
    }

    /**
     * Get price method with parameter: what environment is involved?
     *
     * @param symbolName - Some symbol
     * @param isTestingEnv - run method with in test or prod environment?
     * @return Symbol object
     */
    @Override
    public Symbol getPrice(final String symbolName,
                           final boolean isTestingEnv) {

        Symbol newSymbol;

        synchronized (symbolName) {
            if (!cache.containsKey(symbolName)
                    || Instant.now().minus(
                            CACHE_TIME_SLOT,
                            ChronoUnit.SECONDS)
                    .isAfter(cache.get(symbolName).getTimeStamp())) {

                newSymbol = delegate.getPrice(symbolName, isTestingEnv);
                cache.put(symbolName, newSymbol);

            }

            try {
                Thread.sleep(MILLIS_TO_SLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        return cache.get(symbolName);

    }

}
