package ru.edu;

import ru.edu.model.Symbol;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public final class SymbolToFile {

    private SymbolToFile() {
    }

    /**
     * Path to price log files.
     */
    public static final String PRICE_HISTORY_PATH = "./priceHistory/";

    /**
     * File extension.
     */
    public static final String FILE_EXTENSION = ".txt";

    /**
     * Save Symbol objects and diffs between their prices to file.
     * @param oldSymbol old Symbol object
     * @param newSymbol new Symbol object
     * @throws IOException I/O Exception
     */
    public static void save(final Symbol oldSymbol,
                            final Symbol newSymbol)
            throws IOException {

        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                        .withZone(ZoneId.systemDefault());

        Path path = Paths.get(PRICE_HISTORY_PATH
                + newSymbol.getSymbol()
                + FILE_EXTENSION);

        String temp;
        try (OutputStream out = Files.newOutputStream(path,
                StandardOpenOption.CREATE,
                StandardOpenOption.APPEND,
                StandardOpenOption.WRITE)) {

            temp = formatter.format(newSymbol.getTimeStamp())
                    + "\t|\t" + newSymbol.getPrice()
                    + "\t" + subtract(oldSymbol, newSymbol)
                    + "\n";
            out.write(temp.getBytes());

        }

    }


    private static String subtract(final Symbol oldSymbol,
                                   final Symbol newSymbol) {

        String temp = "";

        if (oldSymbol == null) {
            return "0";
        }

        if (oldSymbol.getSymbol()
                .compareTo(newSymbol.getSymbol()) != 0) {
            return "0";
        }

        if (oldSymbol.getPrice()
                .compareTo(newSymbol.getPrice()) == 0) {
            return "0";
        }

        if (oldSymbol.getPrice().
                compareTo(newSymbol.getPrice()) < 0) {
            temp = "+";
        }

        return temp + newSymbol.getPrice()
                .subtract(oldSymbol.getPrice());

    }

}
