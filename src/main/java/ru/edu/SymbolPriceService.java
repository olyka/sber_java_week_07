package ru.edu;

import ru.edu.model.Symbol;

/**
 * Сервис для получения котировок, должен быть thread-safe.
 *
 */
public interface SymbolPriceService {

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName - Some symbol
     * @return Symbol object
     */
    Symbol getPrice(String symbolName);


    /**
     * Get price method with parameter: what environment is involved?
     * @param someCurrencySymbol - Some symbol
     * @param isTestingEnv - run method with in test or prod environment?
     * @return Symbol object
     */
    Symbol getPrice(String someCurrencySymbol, boolean isTestingEnv);

}
