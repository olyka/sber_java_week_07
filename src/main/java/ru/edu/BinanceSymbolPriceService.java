package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import java.time.Instant;

/**
 * Get info from Binance.com using API and ticker price service endpoint.
 */
public class BinanceSymbolPriceService implements SymbolPriceService {

    /**
     * Logger.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(BinanceSymbolPriceService.class);

    /**
     * Binance API parameters.
     * - API's domain name.
     * - Market Data endpoints: Symbol price ticker.
     * - Symbol price ticker's endpoint parameter.
     */
    public static final String[] API_BINANCE_PROD = {
            "https://api.binance.com",
            "/api/v3/ticker/price",
            "symbol"
    };

    /**
     * Test API parameters.
     * - API's domain name.
     * - Market Data endpoints: Symbol price ticker.
     * - Symbol price ticker's endpoint parameter.     *
     */
    public static final String[] API_BINANCE_TEST = {
            "http://test.ru",
            "/api/v3/ticker/price",
            "symbol"
    };



    /**
     * Error message.
     */
    public static final String FINAL_ERROR_MESSAGE =
            "Binance.com API is not working properly. "
                    + "Try again later. Error message: ";

    /**
     * Error message.
     */
    public static final String ERROR_MESSAGE =
            "Your request has got some errors. Response status code: ";



    /**
     * RestTemplate object.
     */
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName Cryptocurrency symbol
     * @return Symbol object
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        return getPrice(symbolName, false);
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName Cryptocurrency symbol
     * @return Symbol object
     */
    @Override
    public Symbol getPrice(final String symbolName,
                           final boolean isTestEnv) {

        String[] apiBinance = API_BINANCE_PROD;
        if (isTestEnv) {
            apiBinance = API_BINANCE_TEST;
        }

        try {
            ResponseEntity<SymbolImpl> response =
                    restTemplate.getForEntity(
                            apiBinance[0]
                                    + apiBinance[1]
                                    + "?" + apiBinance[2]
                                    + "=" + symbolName,
                            SymbolImpl.class);

            SymbolImpl symbol = response.getBody();
            symbol.setTimestamp(Instant.now());

            LOGGER.info("Binance API Answer for '{}' is {}",
                    symbolName,
                    symbol);
            return symbol;

        } catch (HttpStatusCodeException e) {
            String errorPayLoad = e.getResponseBodyAsString();
            LOGGER.error(ERROR_MESSAGE + " {} {}",
                    e.getStatusCode(),
                    errorPayLoad);
            throw new RuntimeException(ERROR_MESSAGE + e.getStatusCode()
                    + " " + errorPayLoad);

        } catch (RestClientException e) {
            String errorPayLoad = e.getMessage();
            LOGGER.error(FINAL_ERROR_MESSAGE + " {}", errorPayLoad);
            throw new RuntimeException(FINAL_ERROR_MESSAGE + errorPayLoad);
        }

    }
}
